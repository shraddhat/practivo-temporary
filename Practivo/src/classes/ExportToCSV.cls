Public Class ExportToCSV
{
    public string header {get;set;}
    public list<Event> appointmentLst {get;set;}
    public static string lastId {get;set;}
    public ExportToCSV()
    {      
        appointmentLst = new list<Event>();
        header = 'Id, Description, Appointment Type Name, Location, Location Id, Category, Category Id, Service Provider, Service Provider Id, Subject, Appointment StartDateTime, Appointment EndDateTime, WhoId, Booked Resource Id,Description ,Event_Type , No_of_Participants \r\n';                     
        appointmentLst= [SELECT Id,Description,Appointment_Type__r.Name,Location_Category_Mapping__r.Location__r.Name,Location_Category_Mapping__r.Location__r.ID,
                                Location_Category_Mapping__r.Category__r.name,Location_Category_Mapping__r.Category__r.Id,Location_Category_Mapping__r.Contact__r.Name,
                                Location_Category_Mapping__r.Contact__r.Id,Subject,StartDateTime,EndDateTime, WhoId, WhatId, Description__c, Event_Type__c, 
                                No_of_Participants__c FROM EVENT WHERE ( Event_Type__c= :Utils.EventType_Appointment OR Event_Type__c= :Utils.EventType_Partial OR 
                                Event_Type__c= :Utils.EventType_Unavailable OR Event_Type__c= :Utils.EventType_Single_Resource_Booking)  ORDER BY ID LIMIT 50000
                        ];
        lastId =  appointmentLst.get(appointmentLst.size()-1).Id;               
    }
   
    public void exportToCSV()
    {
    
            if(appointmentLst.size()==50000)
            {
                list<event> lst = [SELECT Id,Description,Appointment_Type__r.Name,Location_Category_Mapping__r.Location__r.Name,Location_Category_Mapping__r.Location__r.ID,
                                        Location_Category_Mapping__r.Category__r.name,Location_Category_Mapping__r.Category__r.Id,Location_Category_Mapping__r.Contact__r.Name,
                                        Location_Category_Mapping__r.Contact__r.Id,Subject,StartDateTime,EndDateTime, WhoId, WhatId, Description__c, Event_Type__c, 
                                        No_of_Participants__c FROM EVENT WHERE ( Event_Type__c= :Utils.EventType_Appointment OR Event_Type__c= :Utils.EventType_Partial OR 
                                        Event_Type__c= :Utils.EventType_Unavailable OR Event_Type__c= :Utils.EventType_Single_Resource_Booking) AND  ID > :lastId LIMIT 50000
                                ];
                                
                appointmentLst.addAll(lst);
            }                          
                                                 
    }             
}