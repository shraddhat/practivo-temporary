@istest
public class Test_ViewWaitListController 
{
    static testmethod void testvwcntrl()
    {
        Account acct = new Account( Name = 'Test Account' );
                        insert acct;
        Contact con =  TestDataUtils.getCustomer();
        Contact con1 =  new Contact();
        con1.AccountId=acct.id;
        con1.FirstName = 'Anila';
        con1.LastName = 'Dutta';
        con1.Email = 'anila@swiftsestup.com';
        insert con1;
      
        Event e = new Event();
        e.Subject = 'Test Event';
        e.Location = 'Test Location';
        e.Description = 'Test Description'; 
        e.StartDateTime = datetime.newInstance(2014, 9, 15, 12, 30, 0);
        e.EndDateTime = datetime.newInstance(2014, 9, 15, 13, 30, 0);
        e.Event_Type__c='Waiting';
        e.WhoId=con.id;
        insert e;
        List<event> evlsit=new List<event>();
        Event e1=TestDataUtils.geteventappointment(con1);
        evlsit.add(e);
        evlsit.add(e1);
        EventRelation er = new EventRelation(EventId =e.id,
            RelationId = con1.Id);
            insert er;
        test.startTest();
        ViewWaitListController cvw= new ViewWaitListController();
        System.assertEquals(true,ViewWaitListController.listcontacts.size()>0);
        cvw.geteventwrapperlist();
        String Response=ViewWaitListController.geteventwrapperlistjson();
        System.assertNotEquals('',Response);
        //ViewWaitListController.getFilteredeventwrapperlistjson(evlsit);
        
        cvw.evId=string.valueOf(e.id);
        
        test.stopTest();
    }
}