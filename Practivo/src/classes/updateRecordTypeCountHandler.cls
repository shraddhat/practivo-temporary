public with sharing class updateRecordTypeCountHandler {
map<Id,Resource_Type__c> resTypeMap;
private void initialiseMap()
{
    resTypeMap = new map<Id,Resource_Type__c>([SELECT id,No_of_Resources__c FROM Resource_Type__c LIMIT 2000]);
}
public updateRecordTypeCountHandler(){}
public void updateRecordTypeCount(list<sObject> lst , string objectName)
{
    list<contact> contactLst ;
    list<Resources__c> resourceLst;
    initialiseMap();
    if(objectName =='Contact')
    {
        contactLst=lst;
        for(Contact contactObj : contactLst)
        {
            if(contactObj.Resource_Type__c!=null && contactObj.consideredForCount__c!=true)
            {
                updateCount(contactObj.Resource_Type__c);
                contactObj.consideredForCount__c = true;        
            }   
        }
    }   
    else if(objectName=='Resource')
    {   
        resourceLst = lst;  
        for(Resources__c resourcesObj : resourceLst)
        {
            if(resourcesObj.Resource_Type__c!=null && resourcesObj.consideredForCount__c!=true)
            {
                updateCount(resourcesObj.Resource_Type__c);
                resourcesObj.consideredForCount__c = true;
            }   
        }
    }
    updateResourceType();
            
}    
private void updateResourceType()
{
    list<Resource_Type__c> lstToUpdate = resTypeMap.Values();
    update lstToUpdate;
}
private void updateCount(Id resTypeObjId)
{
    Resource_Type__c resTypeObj = new Resource_Type__c();
    if(resTypeMap.containsKey(resTypeObjId))
    {
        resTypeObj = resTypeMap.get(resTypeObjId);
        if(resTypeObj.No_of_Resources__c==null)
            resTypeObj.No_of_Resources__c=0;
        resTypeObj.No_of_Resources__c = resTypeObj.No_of_Resources__c+1;
        resTypeMap.put(resTypeObjId,resTypeObj);
    }
}
}