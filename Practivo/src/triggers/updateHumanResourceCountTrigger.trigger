trigger updateHumanResourceCountTrigger on Contact (after insert,after update,before delete) 
{
    try
    {
        map<id,List<Contact>> restyperesourceMap=new map<id,List<Contact>>();
        map<id,List<Contact>> restyperesourceMapOld=new map<id,List<Contact>>();
        if (Trigger.isBefore) 
        {
            if (Trigger.isDelete) 
            {
                 Map<ID,Schema.RecordTypeInfo> rt_Map = Contact.sObjectType.getDescribe().getRecordTypeInfosById();
                for (Contact res : Trigger.old) 
                {
                    if(rt_map.get(res.recordTypeID).getName()=='Service Provider' || rt_map.get(res.recordTypeID).getName()=='Assistant')
                    {
                        list<Contact> lst;
                                if(restyperesourceMap.get(res.Resource_Type__c)==null)
                                {
                                    lst = new list<Contact>();
                                    lst.add(res);
                                    restyperesourceMap.put(res.Resource_Type__c,lst);
                                }
                                else
                                    restyperesourceMap.get(res.Resource_Type__c).add(res);
                    }
                } 
                List<Resource_Type__c> RtypeList=[SELECT Id,Name,Type_of_Resource__c,Total_No_of_Resources__c,Total_Overbooking_Limit__c FROM Resource_Type__c WHERE Id IN:restyperesourceMap.keySet()];
                for(Resource_Type__c rtyp:RtypeList)
                {
                    list<Contact> lst;
                    Decimal count=0;
                   if(restyperesourceMap.containsKey(rtyp.Id))
                   {
                       lst=restyperesourceMap.get(rtyp.Id);
                       for(Contact rs:lst)
                       {
                          count=count+rs.Over_Booking_Limit__c;
                       }
                      rtyp.Total_No_of_Resources__c=rtyp.Total_No_of_Resources__c-lst.size();
                      rtyp.Total_Overbooking_Limit__c=rtyp.Total_Overbooking_Limit__c-count;    
                       System.debug('rtyp.Total_No_of_Contact--'+rtyp.Total_No_of_Resources__c);
                   }
                }
                UPDATE RtypeList;
            }
        }
        else if(Trigger.isAfter)
        {
            if (Trigger.isInsert) 
            {
                Map<ID,Schema.RecordTypeInfo> rt_Map = Contact.sObjectType.getDescribe().getRecordTypeInfosById();
                for (Contact res : Trigger.new) 
                {
                    if(rt_map.get(res.recordTypeID).getName()=='Service Provider' || rt_map.get(res.recordTypeID).getName()=='Assistant')
                    {
                        System.debug('In');
                        list<Contact> lst;
                            if(restyperesourceMap.get(res.Resource_Type__c)==null)
                            {
                                lst = new list<Contact>();
                                lst.add(res);
                                restyperesourceMap.put(res.Resource_Type__c,lst);
                            }
                            else
                                restyperesourceMap.get(res.Resource_Type__c).add(res);
                    }
                } 
                List<Resource_Type__c> RtypeList=[SELECT Id,Name,Type_of_Resource__c,Total_No_of_Resources__c,Total_Overbooking_Limit__c FROM Resource_Type__c WHERE Id IN:restyperesourceMap.keySet()];
                for(Resource_Type__c rtyp:RtypeList)
                {
                    list<Contact> lst;
                    Decimal count=0;
                   if(restyperesourceMap.containsKey(rtyp.Id))
                   {
                       lst=restyperesourceMap.get(rtyp.Id);
                       for(Contact rs:lst)
                       {
                          count=count+rs.Over_Booking_Limit__c;
                       }
                      rtyp.Total_No_of_Resources__c=rtyp.Total_No_of_Resources__c+lst.size();
                      rtyp.Total_Overbooking_Limit__c=rtyp.Total_Overbooking_Limit__c+count;    
                       System.debug('rtyp.Total_No_of_Contact--'+rtyp.Total_No_of_Resources__c);
                   }
                }
                UPDATE RtypeList;
            }
            else if(Trigger.isUpdate)
            {
               Map<ID,Schema.RecordTypeInfo> rt_Map = Contact.sObjectType.getDescribe().getRecordTypeInfosById();
                for (Contact res : Trigger.new) 
                {
                    if(rt_map.get(res.recordTypeID).getName()=='Service Provider' || rt_map.get(res.recordTypeID).getName()=='Assistant')
                    {
                        System.debug('In');
                        list<Contact> lst;
                            if(restyperesourceMap.get(res.Resource_Type__c)==null)
                            {
                                lst = new list<Contact>();
                                lst.add(res);
                                restyperesourceMap.put(res.Resource_Type__c,lst);
                            }
                            else
                                restyperesourceMap.get(res.Resource_Type__c).add(res);
                    }
                } 
                
                for (Contact res : Trigger.Old) 
                {
                    if(rt_map.get(res.recordTypeID).getName()=='Service Provider' || rt_map.get(res.recordTypeID).getName()=='Assistant')
                    {
                        System.debug('In');
                        list<Contact> lst;
                            if(restyperesourceMapOld.get(res.Resource_Type__c)==null)
                            {
                                lst = new list<Contact>();
                                lst.add(res);
                                restyperesourceMapOld.put(res.Resource_Type__c,lst);
                            }
                            else
                                restyperesourceMapOld.get(res.Resource_Type__c).add(res);
                    }
                }
                
                List<Resource_Type__c> RtypeList=[SELECT Id,Name,Type_of_Resource__c,Total_No_of_Resources__c,Total_Overbooking_Limit__c FROM Resource_Type__c WHERE Id IN:restyperesourceMap.keySet()];
                for(Resource_Type__c rtyp:RtypeList)
                {
                    list<Contact> lst;
                    list<Contact> lstOld;
                    Decimal count=0;
                    Decimal countOld=0;
                    Decimal Diff;
                   if(restyperesourceMap.containsKey(rtyp.Id) && restyperesourceMapOld.containsKey(rtyp.Id))
                   {
                       lst=restyperesourceMap.get(rtyp.Id);
                       lstOld=restyperesourceMapOld.get(rtyp.Id);
                       for(Contact rs:lst)
                       {
                          count=count+rs.Over_Booking_Limit__c;
                       }
                       for(Contact rs:lstOld)
                       {
                          countOld=countOld+rs.Over_Booking_Limit__c;
                       }
                      if(countOld>count)
                      { 
                          Diff=countOld-count;
                          rtyp.Total_Overbooking_Limit__c=rtyp.Total_Overbooking_Limit__c-Diff;  
                      }
                      else if(count>countOld)
                      {
                         Diff=count-countOld;
                          rtyp.Total_Overbooking_Limit__c=rtyp.Total_Overbooking_Limit__c+Diff; 
                      }
                   }
                }
                UPDATE RtypeList; 
            }
        }
  }  
    catch(Exception e)
    {
        System.debug('updateResourceCountTriger-->'+e.getMessage()+e.getStackTraceString());
    }
}